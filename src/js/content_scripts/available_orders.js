function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function getId() {
  return getRandomInt(1, 100000000);
}

let running = false;
const portId = `${getId()}`;

function sendMessage(message) {
  port.postMessage({...message, portId: portId});
}

const port = chrome.runtime.connect({name: portId});
sendMessage({messageType: 'addTab', href: window.location.href});

port.onMessage.addListener((msg) => {
  if (msg.messageType === 'stop') {
    running = false;
  } else if (msg.messageType === 'isRunning') {
    running = msg.isRunning;
    processOrders();
  }
});

function processOrders() {
  // const moreOptionsItem = document.getElementById('more_filters');
  const onlyOrdersItem = document.getElementById('sts_order');
  const newOrdersItem = document.getElementById('new_order');
  const formSubmitBtn = document.getElementById('filterSubmit');
  let timerId = null;

  /*
  if (moreOptionsItem) {
    moreOptionsItem.click();
  }
  */

  if (onlyOrdersItem) {
    onlyOrdersItem.checked = true;
  }

  if (newOrdersItem) {
    newOrdersItem.checked = true;
  }

  if (formSubmitBtn) {
    setTimeout(checkOrders, 1000, timerId, formSubmitBtn);
  }
}

function foundOrder(orderId, orderUrl) {
  sendMessage({messageType: 'foundOrder', orderId: orderId, orderUrl: orderUrl});
}

function openOrders(timerId, submitBtn) {
  const availableOrders = document.querySelectorAll('#available_orders_list .order-link');
  availableOrders.forEach((orderNode) => {
    const childUl = orderNode.getElementsByTagName('ul')[0];
    const orderId = childUl.dataset.order_id;
    const href = orderNode.getAttribute('href');
    if (href !== null && href !== '') {
      const orderUrl = `${window.location.protocol}//${window.location.hostname}${href}`;
      foundOrder(orderId, orderUrl);
    }
  });
  timerId = null;
  checkOrders(timerId, submitBtn);
}

function checkOrders(timerId, submitBtn) {
  if (timerId === null && running) {
    setTimeout(openOrders, 800, timerId, submitBtn);
    submitBtn.click();
  }
}
