let running = false;
const portId = `${getId()}`;

const port = chrome.runtime.connect({name: portId});
sendMessage({messageType: 'addTab'});

port.onMessage.addListener((msg) => {
  if (msg.messageType === 'stop') {
    running = false;
  } else if (msg.messageType === 'isRunning') {
    running = msg.isRunning;
    if (running) {
      orderSubmit();
    }
  }
});

function sendMessage(message) {
  port.postMessage({...message, portId: portId});
}

function orderSubmit() {
  const take = document.querySelector('input[value="Take Order"]');
  if (take !== null) {
    chrome.runtime.sendMessage({messageType: 'orderTaken'});
    take.click();
  }
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function getId() {
  return getRandomInt(1, 100000000);
}
