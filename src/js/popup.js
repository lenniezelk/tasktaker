const startBtn = document.getElementById('startBtn');
const endBtn = document.getElementById('endBtn');

const onStart = () => {
  chrome.runtime.sendMessage({messageType: 'startRequested'});
};

const onStop = () => {
  chrome.runtime.sendMessage({messageType: 'stopRequested'});
}

startBtn.onclick = onStart;
stopBtn.onclick = onStop;
