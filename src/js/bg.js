let isRunning = false;
let orderIds = {};
let ports = {};
const availablePage = 'https://www.uvocorp.com/orders/available.html';
const failedAudio = new Audio('../audio/failed.mp3');
const takenAudio = new Audio('../audio/taken.mp3');


chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.messageType === 'stopRequested') {
      requestStop();
      isRunning = false;
      orderIds = {};
      ports: {};
  } else if (request.messageType === 'startRequested') {
      if (isRunning) {
        /*
        chrome.tabs.query({url: availablePage}, (tabs) => {
          tabs.forEach((tab) => {
            chrome.tabs.remove(tab.id);
          });
          chrome.tabs.create({url: availablePage, active: true});
        });
        */
        chrome.tabs.create({url: availablePage, active: true});
      }
      else {
        isRunning = true;
        orderIds = {};
        ports: {};
        chrome.tabs.create({url: availablePage, active: true});
      }
  } else if (request.messageType === 'notLoggedIn') {
    const options = {type: 'basic', title: 'Task Taker', message: 'User not logged in', iconUrl: '../icons/taker_48.png'};
    chrome.notifications.create('', options);
    failedAudio.play();
  } else if (request.messageType === 'orderTaken') {
    const options = {type: 'basic', title: 'Task Taker', message: 'Order taken', iconUrl: '../icons/taker_48.png'};
    chrome.notifications.create('', options);
    takenAudio.play();
  }
});

chrome.runtime.onConnect.addListener((port) => {
  port.onDisconnect.addListener((port) => {
    delete ports[port.name];
  });

  port.onMessage.addListener((msg) => {
    if (msg.messageType === 'addTab') {
      ports[port.name] = port;
      sendIsRunningStatus(port);
    } else if (msg.messageType === 'foundOrder') {
      const orderId = msg.orderId;
      const orderUrl = msg.orderUrl;
      if (!orderIds.hasOwnProperty(orderId)) {
        applyOrder(orderUrl);
        orderIds[orderId] = '';
      }
    }
  });
});

function applyOrder(orderUrl) {
  chrome.tabs.create({url: orderUrl, active: false});
}

function sendIsRunningStatus(port) {
  port.postMessage({messageType: 'isRunning', isRunning: isRunning});
}

function requestStop() {
  const message = {messageType: 'stop'};

  for (let portId in ports) {
    const port = ports[portId];
    port.postMessage(message);
  }
}
